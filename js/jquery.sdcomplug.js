;(function($) {
	$.fn.extend({
		"AD": function(options){
				options = $.extend({
				adid : [0],
				AdUrlHead : 'http://www.shandongbusiness.gov.cn'
			},options);
			var addiv = this;
			var AdImgPathHead;
			var AdImgpath;
			var AdUrl;
			var adHtml;
			$.get("./public.php",{adid:options.adid},function(data){
				data = JSON.parse(data);// 可删除
				if(data.ads.length === 1){
					AdImgPathHead = options.AdUrlHead;
					AdImgpath = data.ads[0].path;
					AdUrl = data.ads[0].url;
					//addiv.find('img').attr('src',AdImgPathHead+AdImgpath);
					//addiv.find('a').attr('href',AdUrl);
					addiv.empty();
					adHtml = '<a href="#" target="_blank"><img src="'+ AdImgPathHead+AdImgpath +'" width="100%" alt=""></a><span><a href="'+ AdUrl +'" target="_blank"><i class="fa fa-arrow-right" aria-hidden="true"></i>进入专题</a></span>';
					addiv.append(adHtml);
				}
			});
		},
		"img_center": function(){
			var img_box = this;
			var img_list = img_box.find('img');
			
			for(var i=0; i<img_list.length; i++){
				var img_m = $(img_list[i]);//图片主体
				var img_w = img_m.width();//图片的宽度
				var img_h = img_m.height();//图片的高度
				var img_p = img_m.parent();//图片的外容器
				var img_p_w = img_p.width();//图片的外容器宽度
				var img_p_h = img_p.height();//图片的外容器高度
				if(img_w/img_h <= img_p_w/img_p_h){
					//将图片宽度放大到容器宽度，高度多余的溢出隐藏，上部隐藏为溢出的一半
					img_m.width(img_p_w);
					img_h = img_m.height();
					img_m.css('marginTop',(img_p_h-img_h)/2);
	
				}else{
					img_m.height(img_p_h);
					img_w = img_m.width();
					img_m.css('marginLeft',(img_p_w-img_w)/2);
	
				}
			}
		}
		
	});
})(jQuery);