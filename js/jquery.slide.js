$.fn.imgSlide = function(options){
	var defaults = {
		'speed':'2000',//选项卡切换时间间隔
		'add_class':'add_class'//new_title_trigger
	};
	var settings = $.extend({},defaults,options);// 合并参数对象
	
	var slideImgList = this.children('.img_new_list');// 幻灯片主体
	var slideIconList = this.children('.img_new_icon');// 幻灯片按钮
	var slide_num = 0;//当前触发选项卡索引
	var slide_len = slideIconList.find('li').length;// 选项卡数量
	
	slideIconList.find('li:first span').addClass(settings.add_class);//设置首个分类默认样式

	function donghu_fn(){// 自动切换函数
		//console.log(tab_num);
		if(slide_num === slide_len){//如果当前触发的是最后一个，则将索引重置为0
			slide_num = 0;
		}
		slideIconList.find('li').eq(slide_num).siblings().children('span').removeClass(settings.add_class);
		slideIconList.find('li').eq(slide_num).children('span').addClass(settings.add_class);
		slideImgList.find('li').eq(slide_num).siblings().hide();
		slideImgList.find('li').eq(slide_num).show();
		slide_num++;
	}
	
	var dh = setInterval(donghu_fn,settings.speed);// 创建动画，第一个参数是动画函数，第二个参数是传进来的时间间隔参数
	
	function slide_add_class(){// 鼠标指上添加触发样式
		clearInterval(dh);//每次指上后都先清除动画指针
		$(this).siblings().children('span').removeClass(settings.add_class);
		$(this).children('span').addClass(settings.add_class);
		slide_num = $(this).index();//根据标题设置触发的选项卡索引
		slideImgList.find('li').eq(slide_num).siblings().hide();
		slideImgList.find('li').eq(slide_num).show();
		//console.log(tab_num);
	}
	
	slideIconList.find('li').mouseover(slide_add_class);//为所有的标题添加事件监听
	
	$(this).hover(
		function(){
			clearInterval(dh);//指上后清除动画
		},
		function(){
			dh = setInterval(donghu_fn,settings.speed);//移开后重新添加动画指正
		}
	);
};