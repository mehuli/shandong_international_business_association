$.fn.swtTab = function(options){
	var defaults = {
		'speed':'2000',//选项卡切换时间间隔
		'add_class':'add_class',//new_title_trigger
	}
	var settings = $.extend({},defaults,options);// 合并参数对象
	
	var tab_title = this.children('.tab_title');// 选项卡标题
	var tab_main = this.children('.tab_main');// 选项卡主体
	var tab_num = 0;//当前触发选项卡索引
	var tab_len = tab_title.find('li').length;// 选项卡数量
	
	tab_title.find('li:first').addClass(settings.add_class);//设置首个分类默认样式
	
	function donghu_fn(){// 自动切换函数
		if(tab_num == tab_len){//如果当前触发的是最后一个，则将索引重置为0
			tab_num = 0;
		};
		tab_title.find('li').eq(tab_num).siblings().removeClass(settings.add_class);
		tab_title.find('li').eq(tab_num).addClass(settings.add_class);
		tab_main.children('div').eq(tab_num).siblings().hide();
		tab_main.children('div').eq(tab_num).show();
		tab_num++;
	};

	var dh = setInterval(donghu_fn,settings.speed);// 创建动画，第一个参数是动画函数，第二个参数是传进来的时间间隔参数
	
	function tab_add_class(){// 鼠标指上添加触发样式
		clearInterval(dh);//每次指上后都先清除动画指针
		$(this).siblings().removeClass(settings.add_class);
		$(this).addClass(settings.add_class);
		tab_num = $(this).index();//根据标题设置触发的选项卡索引
		tab_main.children('div').eq(tab_num).siblings().hide();
		tab_main.children('div').eq(tab_num).show();
	};
	tab_title.find('li').mouseover(tab_add_class);//为所有的标题添加事件监听
	
	$(this).hover(
		function () {
			clearInterval(dh);//指上后清除动画
		},
		function () {
			dh = setInterval(donghu_fn,settings.speed);//移开后重新添加动画指正
		}
	);
}
$.fn.Tab_switch = function(options){
	var defaults = {
		'add_class':'add_class',//new_title_trigger
	}
	var settings = $.extend({},defaults,options);// 合并参数对象
	
	var tab_title = this.children('.tab_title');// 选项卡标题
	var tab_main = this.children('.tab_main');// 选项卡主体
	var tab_num = 0;//当前触发选项卡索引
	var tab_len = tab_title.find('li').length;// 选项卡数量
	
	tab_title.find('li:first').addClass(settings.add_class);//设置首个分类默认样式
	
	function tab_add_class(){// 鼠标指上添加触发样式
		$(this).siblings().removeClass(settings.add_class);
		$(this).addClass(settings.add_class);
		tab_num = $(this).index();//根据标题设置触发的选项卡索引
		tab_main.children('div').eq(tab_num).siblings().hide();
		tab_main.children('div').eq(tab_num).show();

	};
	tab_title.find('li').mouseover(tab_add_class);//为所有的标题添加事件监听
}